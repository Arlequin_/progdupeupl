# README

Progdupeupl is a community of French programmers.

## Naming conventions

Everything in the code should be in english (vars, funcs, methods, docstrings,
comments) though some parts may - of course - be in French.

## Dependencies

To install all dependencies, simply run `pip install -r requirements.txt`.

## Copyright

Progdupeupl is brought to you under GNU GPLv3 licence. For further informations
read the COPYING file. This project use some code parts from progmod
(http://progmod.org) avaible under the MIT licence.
